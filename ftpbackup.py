#!/usr/bin/python
# -*- coding: UTF-8 -*-

import ftplib
import os
import time

#####################################################
### CONFIG
#####################################################
#SQL
sqlUser = ""
sqlPass = ""

#DIRWWWW
dirWWWFiles = ""

#FTP
ftpHost = ""
ftpPort = 21
ftpLogin = ""
ftpPass = ""
ftpCountBackups = 3 #ilość backup'ow przechowywanych na zewnetrznym serwerze

#####################################################
### PROGRAM
#####################################################

dataAktualna = time.strftime("%Y%m%d", time.localtime())
backupFolderName = str(dataAktualna)+"_backup"

# tworzenie folderow do backupu
print "tworze folder backup"
os.system("mkdir "+backupFolderName)
print "tworze folder backup/wwwFiles"
os.system("mkdir "+backupFolderName+"/wwwFiles")

# kopiowanie plikow
print "kopiuje pliki do wwwFiles"
os.system("cp -R "+dirWWWFiles+"* "+backupFolderName+"/wwwFiles/")

# backup mysql
print "tworze backup baz danych"
os.system("mysqldump -u "+sqlUser+" --password="+sqlPass+" --all-databases > "+backupFolderName+"/backup.sql")
print "mysqldump -u "+sqlUser+" --password="+sqlPass+" --all-databases > "+backupFolderName+"/backup.sql"

# archiwizacja folderu
print "tworze archiwum"
os.system("tar -czf "+backupFolderName+".tar.gz "+backupFolderName)




# FTP
print "FTP:"
ftp = ftplib.FTP()
ftp.connect(ftpHost, ftpPort)
print (ftp.getwelcome())

print ("FTP: Logowanie")
ftp.login(ftpLogin, ftpPass)
print "FTP: Zalogowano"

print "sprawdzanie czy nie za duzo backupow"
ftp.cwd("/")

files = []
tempFiles = []

ftp.retrlines('MLSD', files.append)

for file in files:
    tempFile = file.split(";")

    if tempFile[0] == 'type=file':
        temp = tempFile[7]
        temp = temp.replace(" ","")
        tempFiles.append(temp)


if len(tempFiles) >= ftpCountBackups:
    print "FTP: za duzo backupow ("+str(ftpCountBackups)+") - nalezy jakis usunac"
    tempFiles.sort()
    ftp.delete(tempFiles[0])
    print "FTP: usunieto najstarszy backup"


print "FTP: Upload w toku"
file = open(backupFolderName+".tar.gz", "rb")
ftp.storbinary('STOR ' + backupFolderName + ".tar.gz", file)
ftp.quit()
file.close()
print "FTP: Upload pomyslny"

# czyszczenie
print "Czyszczenie"
os.system("rm -r "+backupFolderName+"/")
os.system("rm -r "+backupFolderName+".tar.gz")

print "Done."
